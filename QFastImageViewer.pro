TEMPLATE = app

QT += qml quick
CONFIG += c++11
TARGET = qfastiv

INCLUDEPATH += /usr/include/qt/QtQuick/$$QT_VERSION/QtQuick
INCLUDEPATH += /usr/include/qt/QtQuick/$$QT_VERSION/QtQuick/private

HEADERS += \
    src/filemanager.h \
    src/filemanager_p.h \
    src/imageeditor.h \
    src/imageeditor_p.h \
    src/imageprovider.h \
    src/imageprovider_p.h

SOURCES += \
    src/main.cpp \
    src/filemanager.cpp \
    src/imageeditor.cpp \
    src/imageprovider.cpp

RESOURCES += src/resources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    README.md

