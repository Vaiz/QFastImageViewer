#ifndef IMAGEEDITOR_P
#define IMAGEEDITOR_P

#include "imageeditor.h"

#include <QFileInfo>

class ImageEditorPrivate
{
    Q_DECLARE_PUBLIC(ImageEditor)

public:
    ImageEditorPrivate(ImageEditor *q);

    QFileInfo fileInfo;
    QImage image;
    bool edited;
    bool readOnly;

    ImageEditor * const q_ptr;
};

#endif // IMAGEEDITOR_P

