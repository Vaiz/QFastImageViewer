#ifndef FILEMANAGER_P_H
#define FILEMANAGER_P_H

#include "filemanager.h"

class FileManagerPrivate
{
    Q_DECLARE_PUBLIC(FileManager)

public:
    FileManagerPrivate(FileManager *q);

    QString location;
    QString fileName;

    FileManager * const q_ptr;
};

#endif // FILEMANAGER_P_H

