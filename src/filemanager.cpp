#include "filemanager.h"
#include "filemanager_p.h"

#include <QGuiApplication>
#include <QQmlEngine>
#include <QFileInfo>
#include <QDebug>
#include <QDir>

#include <QImage>

FileManagerPrivate::FileManagerPrivate(FileManager *q)
    : q_ptr(q)
{
    if(qApp->arguments().size() <= 1)
        location = QDir::homePath();
    else
    {
        QFileInfo fileInfo(qApp->arguments()[1]);
        location = fileInfo.absolutePath();
        fileName = fileInfo.fileName();
    }
}

FileManager::FileManager(QObject *parent)
    : QObject(parent),
      d_ptr(new FileManagerPrivate(this))
{
}

FileManager::~FileManager()
{
    delete d_ptr;
}

QObject *FileManager::qmlSingleton(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static FileManager *object = new FileManager();
    return object;
}

QString FileManager::location() const
{
    Q_D(const FileManager);
    return d->location;
}

void FileManager::setLocation(const QString &location)
{
    Q_D(FileManager);
    d->location = location;
    setFileName("");
    emit locationChanged();
}

QString FileManager::fileName() const
{
    Q_D(const FileManager);
    return d->fileName;
}

void FileManager::setFileName(const QString &fileName)
{
    Q_D(FileManager);
    d->fileName = fileName;
    emit fileNameChanged();
}

QVariantList FileManager::entryList() const
{
    QDir qDir(location());

    QVariantList variantList;
    {
        QVariantMap variantMap;
        variantMap.insert("icon", "qrc:/icons/up_512x512.png");
        variantMap.insert("isDir", true);
        variantMap.insert("name", "..");
        variantMap.insert("isGif", false);
        variantList += variantMap;
    }

    QFileInfoList dirs = qDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);
    for(const QFileInfo &dir : dirs)
    {
        QVariantMap variantMap;
        variantMap.insert("icon", "qrc:/icons/folder_512x512.png");
        variantMap.insert("isDir", true);
        variantMap.insert("name", dir.fileName());
        variantMap.insert("isGif", false);
        variantList += variantMap;
    }

    QStringList imageFilter;
    imageFilter << "*.png" << "*.jpg" << "*.gif" << "*.svg" << "*.ico" << "*.bmp";
    QFileInfoList images = qDir.entryInfoList(imageFilter,
                                              QDir::Files | QDir::Readable, QDir::Name);
    for(const QFileInfo &img : images)
    {
        QVariantMap variantMap;
        variantMap.insert("icon", img.absoluteFilePath());
        variantMap.insert("isDir", false);
        variantMap.insert("name", img.fileName());

        if(img.suffix() == "gif")
            variantMap.insert("isGif", true);
        else
            variantMap.insert("isGif", false);

        variantList += variantMap;
    }
    return variantList;
}

void FileManager::changeDir(const QString &fileName)
{
    QFileInfo fileInfo(location() + "/" + fileName);
    if(!fileInfo.isDir())
    {
        qWarning() << tr("%1 is not directory").arg(fileInfo.absoluteFilePath());
        return;
    }

    QDir dir(location());
    dir.cd(fileName);
    setLocation(dir.absolutePath());
    return;
}
