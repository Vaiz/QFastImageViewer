#include "imageprovider.h"
#include "imageeditor.h"

#include <QPixmap>
#include <QPainter>

ImageProvider::ImageProvider()
    : QQuickImageProvider(QQuickImageProvider::Image)
{
}

QImage ImageProvider::requestImage(const QString &id,
                                   QSize *size,
                                   const QSize &requestedSize)
{
    Q_UNUSED(id);
    Q_UNUSED(requestedSize);

    QImage image = ((ImageEditor *) ImageEditor::qmlSingleton(0,0))->image();
    if(image.isNull())
    {
        int width = 200;
        int height = 200;

        QPixmap pixmap(width, height);
        pixmap.fill(Qt::black);

        QPainter painter(&pixmap);
        painter.setPen(Qt::red);

        QFont font = painter.font();
        font.setPixelSize(height / 4);
        painter.setFont(font);

        painter.drawText(QRect(0, 0, width, height / 2),
                         Qt::AlignHCenter | Qt::AlignVCenter, "Invalid");
        painter.drawText(QRect(0, height / 2, width, height / 2),
                         Qt::AlignHCenter | Qt::AlignVCenter, "image");

        image = pixmap.toImage();
    }
    *size = image.size();

    return image;
}
