#include "imageeditor.h"
#include "imageeditor_p.h"

#include <QGuiApplication>
#include <QQmlEngine>
#include <QDebug>
#include <qquickimagebase_p.h>

ImageEditorPrivate::ImageEditorPrivate(ImageEditor *q)
    : edited(false),
      readOnly(true),
      q_ptr(q)
{
}

ImageEditor::ImageEditor()
    : QObject(0),
      d_ptr(new ImageEditorPrivate(this))
{
}

ImageEditor::~ImageEditor()
{
    delete d_ptr;
}

QObject *ImageEditor::qmlSingleton(QQmlEngine *engine,
                                   QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static ImageEditor *object = new ImageEditor();
    return (QObject *)object;
}

QImage ImageEditor::image() const
{
    Q_D(const ImageEditor);
    return d->image;
}

bool ImageEditor::edited() const
{
    Q_D(const ImageEditor);
    return d->edited;
}

void ImageEditor::setEdited(const bool edited)
{
    Q_D(ImageEditor);

    if(d->edited == edited)
        return;

    d->edited = edited;
    emit editedChanged();
}

bool ImageEditor::readOnly() const
{
    Q_D(const ImageEditor);
    return d->readOnly;
}

void ImageEditor::setReadOnly(const bool readOnly)
{
    Q_D(ImageEditor);

    if(d->readOnly == readOnly)
        return;

    d->readOnly = readOnly;
    emit readOnlyChanged();
}

void ImageEditor::setImage(QObject *image)
{
    Q_D(ImageEditor);
    QQuickImageBase *imageBase = qobject_cast<QQuickImageBase *>(image);

    if(imageBase == 0)
    {
        qDebug() << "ImageEditor: error cast to 'QQuickImageBase *' from "
                 << image->metaObject()->className();
        return;
    }

    d->fileInfo.setFile(imageBase->source().path());
    d->image.load(d->fileInfo.absoluteFilePath());

    if(d->fileInfo.suffix() == "svg" || d->fileInfo.suffix() == "gif")
        setReadOnly(true);
    else
        setReadOnly(false);

    setEdited(false);

    emit imageChanged();
}

void ImageEditor::rotate(const int &degree)
{
    if(degree == 0)
        return;

    Q_D(ImageEditor);

    QMatrix matrix;
    matrix.rotate(degree);

    d->image = d->image.transformed(matrix);
    emit imageChanged();

    setEdited(true);
}

void ImageEditor::mirrored(const bool horizontal,
                           const bool vertical)
{
    if(!horizontal && !vertical)
        return;

    Q_D(ImageEditor);

    d->image = d->image.mirrored(horizontal, vertical);
    emit imageChanged();

    setEdited(true);
}

void ImageEditor::save()
{
    Q_D(const ImageEditor);

    if(!d->edited)
        return;

    if(!d->image.save(d->fileInfo.absoluteFilePath(), 0, 100))
        qDebug() << "Cann't save  "  << d->fileInfo.absoluteFilePath();
    else
        setEdited(false);
}
