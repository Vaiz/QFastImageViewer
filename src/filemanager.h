#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>
#include <QFileInfoList>
#include <QVariant>

class QQmlEngine;
class QJSEngine;
class FileManagerPrivate;

class FileManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString location READ location WRITE setLocation NOTIFY locationChanged)
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName NOTIFY fileNameChanged)

    Q_PROPERTY(QVariantList entryList READ entryList NOTIFY locationChanged)

private:
    explicit FileManager(QObject *parent = 0);

public:
    ~FileManager();
    static QObject *qmlSingleton(QQmlEngine *engine,
                                 QJSEngine *scriptEngine);

    QString location() const;
    void setLocation(const QString &);

    QString fileName() const;
    void setFileName(const QString &);

    QVariantList entryList() const;
    Q_INVOKABLE void changeDir(const QString &);

signals:
    void locationChanged();
    void fileNameChanged();

protected:
    FileManagerPrivate * const d_ptr;

private:
    Q_DECLARE_PRIVATE(FileManager)
};

#endif // FILEMANAGER_H
