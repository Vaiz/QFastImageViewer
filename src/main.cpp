#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "filemanager.h"
#include "imageeditor.h"
#include "imageprovider.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterSingletonType<FileManager>("org.FastImageViewer.FileManager", 1, 0,
                                          "FileManager", FileManager::qmlSingleton);
    qmlRegisterSingletonType<ImageEditor>("org.FastImageViewer.ImageEditor", 1, 0,
                                          "ImageEditor", ImageEditor::qmlSingleton);

    QQmlApplicationEngine engine;
    engine.addImageProvider("imageprovider", new ImageProvider);
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}

