import QtQuick 2.5
import org.FastImageViewer.FileManager 1.0

Item
{
    property var entries: FileManager.entryList
    property ImageListItem currentImage: listView.currentItem

    function next()
    {
        var index = listView.currentIndex + 1;
        setCurrentIndex(index);
    }

    function prev()
    {
        var index = listView.currentIndex - 1;
        setCurrentIndex(index);
    }

    function setCurrentIndex(index)
    {
        if(index >= 0 && index < listView.count && !listView.model[index].isDir)
            listView.currentIndex = index;
    }

    Rectangle
    {
        anchors.fill: parent
        color: "white"
    }

    Component
    {
        id: imagesListComponent

        ImageListItem
        {
            property bool isDir: modelData.isDir

            id: imageListItem
            width: parent.height / 8 * 7;
            height: parent.height

            iconSource:
            {
                if(!modelData.isDir)
                    return "file:" + modelData.icon
                else
                    return modelData.icon
            }
            name: modelData.name
            isGif: modelData.isGif
            cheked: listView.currentIndex == index

            MouseArea
            {
                anchors.fill: parent

                onClicked:
                {
                    if(!modelData.isDir)
                        listView.currentIndex = index;
                }

                onDoubleClicked:
                {
                    if(modelData.isDir)
                        FileManager.changeDir(modelData.name);
                }
            }
        }
    }

    ListView
    {
        id: listView
        anchors.fill: parent
        orientation: ListView.Horizontal
        model: entries
        delegate: imagesListComponent

        onCountChanged:
        {
            // Почему-то вызывается два раза
            for(var i = 0; i < count; ++i)
            {
                if(FileManager.fileName.length != 0)
                {
                    if(model[i].name === FileManager.fileName)
                    {
                        currentIndex = i;
                        break;
                    }
                }
                else if(!model[i].isDir)
                {
                    currentIndex = i;
                    break;
                }
            }
        }
    }
}

