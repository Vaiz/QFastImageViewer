import QtQuick 2.5

Rectangle
{
    property string text: "text"

    id: headerRect
    anchors.left: parent.left
    anchors.right: parent.right
    height: headerText.font.pixelSize * 2
    color: "lightgrey"

    Text
    {
        id: headerText
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 16

        text: headerRect.text
    }
}

