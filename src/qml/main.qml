import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import org.FastImageViewer.FileManager 1.0
import org.FastImageViewer.ImageEditor 1.0

Window
{
    id: mainWindow
    visible: true
    color: "black"
    title: "Fast Image Viewer"
    x: 100;  y: 100; width: 1000; height: 600
    //flags: Qt.FramelessWindowHint // Пропадает и заголовок

    property bool fullscreen: false

    Connections
    {
        target: ImageEditor
        onImageChanged: image.updateSource()
    }

    onHeightChanged: image.updateFillMode()
    onWidthChanged: image.updateFillMode()

    Image
    {
        id: image
        anchors.fill: parent
        visible: !imageList.currentImage.isGif && !imageList.currentImage.isDir
        source: "image://imageprovider/" + uniqueImg;
        onSourceChanged: updateFillMode()

        property int translateX: 0
        property int translateY: 0
        transform: Translate{x: image.translateX; y: image.translateY}

        property int uniqueImg: 0

        function updateSource()
        {
            ++uniqueImg;
            updateFillMode();
        }

        function updateFillMode()
        {
            if(sourceSize.height > height || sourceSize.width > width)
                fillMode = Image.PreserveAspectFit;
            else
                fillMode = Image.Pad;
        }
    }

    AnimatedImage
    {
        anchors.fill: parent
        visible: imageList.currentImage.isGif
        playing: imageList.currentImage.isGif
        source: visible ?  imageList.currentImage.iconSource : ""
        fillMode:
        {
            if(sourceSize.height > height || sourceSize.width > width )
                return Image.PreserveAspectFit;
            else
                return Image.Pad
        }
    }

    Text
    {
        x: 0
        y: 0
        color: "#22FF22"
        font.family: "Liberation Sans"
        font.pointSize: 14
        text:
        {
            if(imageList.currentImage.isDir)
                return qsTr("Изображений нет");
            else
                return imageList.currentImage.iconSource.substring("file:".length);
        }
    }

    MouseArea
    {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor

        property bool isLeftButtonHold: false
        property point startPoint

        onDoubleClicked:
        {
            if(mainWindow.fullscreen)
            {
                mainWindow.showNormal();
                mainWindow.fullscreen = false;
            }
            else
            {
                mainWindow.showFullScreen();
                mainWindow.fullscreen = true;
            }
        }

        onPressAndHold:
        {
            if(pressedButtons & Qt.LeftButton)
            {
                startPoint.x = mouseX;
                startPoint.y = mouseY;
                cursorShape = Qt.BlankCursor;
                image.scale = 2;

                imagesListMouseArea.visible = false;
                leftPanelMouseArea.visible = false;

                isLeftButtonHold = true;
            }
        }

        onReleased:
        {
            if(!(pressedButtons & Qt.LeftButton))
            {
                image.translateX = 0;
                image.translateY = 0;
                cursorShape = Qt.PointingHandCursor;
                image.scale = 1;

                imagesListMouseArea.visible = true;
                leftPanelMouseArea.visible = true;
                isLeftButtonHold = false;
            }
        }

        onPositionChanged:
        {
            if(isLeftButtonHold)
            {
                image.translateX = (mouseX - startPoint.x) * 2;
                image.translateY = (mouseY - startPoint.y) * 2;
            }
        }

        onWheel: imageList.onWheel(wheel.angleDelta);
    }

    Item
    {
        id: shorcuts
        anchors.fill: parent
        focus: true

        Keys.onLeftPressed: imageList.prev()
        Keys.onRightPressed: imageList.next()
        Keys.onEscapePressed:
        {
            if(ImageEditor.edited)
            {
                saveDialog.quitAfterSave = true;
                saveDialog.visible = true;
            }
            else
                Qt.quit();
        }

        Action
        {
            id: saveAction
            shortcut: StandardKey.Save
            onTriggered:
            {
                if(!ImageEditor.edited)
                    return;

                saveDialog.visible = true;
            }
        }
    }

    MouseArea
    {
        id: imagesListMouseArea
        anchors.fill: imageList
        hoverEnabled: true
    }

    ImageList
    {
        id: imageList
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 256

        visible: imagesListMouseArea.containsMouse

        onCurrentImageChanged:
        {
            if(ImageEditor.edited)
            {
                saveDialog.currentImageChanged = true;
                saveDialog.visible = true;
            }
            else
                ImageEditor.setImage(currentImage.image)
        }

        function onWheel(angle)
        {
            if(angle === Qt.point(0, 0))
                return;

            if(angle.y < 0)
                imageList.next();
            else
                imageList.prev();
        }
    }

    MouseArea
    {
        id: leftPanelMouseArea
        anchors.fill: leftPanel
        hoverEnabled: true
    }

    LeftPanel
    {
        id: leftPanel
        anchors.left: parent.left
        y: (mainWindow.height - height) / 2
        width: 300
        height: 200

        visible: leftPanelMouseArea.containsMouse && image.visible && !ImageEditor.readOnly
        onSaveFile: saveDialog.visible = true
    }

    MessageDialog
    {
        id: saveDialog
        property bool currentImageChanged: false
        property bool quitAfterSave: false

        icon: StandardIcon.Question
        title: qsTr("Сохранить изменения?")
        text: qsTr("Сохранить изменения?")
        standardButtons: StandardButton.Yes | StandardButton.No

        onNo:
        {
            if(quitAfterSave)
                Qt.quit();

            if(!currentImageChanged)
                return;

            ImageEditor.setImage(imageList.currentImage.image);
            currentImageChanged = false;           
        }

        onYes:
        {
            ImageEditor.save();
            no();
        }
    }
}
