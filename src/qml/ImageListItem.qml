import QtQuick 2.5

Item
{
    id: imageListItem

    property string iconSource: ""
    property string name: ""
    property bool isGif: false
    property bool cheked: false
    property Image image: imageItem

    Rectangle
    {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: textItem.top

        border.width: imageListItem.cheked ? 4 : 0
        border.color: "blue"

        Image
        {
            id: imageItem
            anchors.fill: parent
            anchors.margins: parent.width / 32

            fillMode: Image.PreserveAspectFit
            source: imageListItem.iconSource
        }
    }

    Text
    {
        id: textItem
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: font.pixelSize * 2

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        wrapMode: Text.Wrap
        maximumLineCount: 1

        font.pixelSize: parent.width / 16
        text: imageListItem.name
        color: "black"
    }
}

