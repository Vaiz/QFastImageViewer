import QtQuick 2.5
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import org.FastImageViewer.ImageEditor 1.0

Rectangle
{
    property int fontPixelSize: 12
    property int rowHeight: fontPixelSize * 2
    signal saveFile

    color: "white"

    ColumnLayout
    {
        anchors.fill: parent
        spacing: 0

        HeaderText
        {
            id: resizeHeader
            Layout.row: 0

            text: qsTr("Поворот, размеры")
        }

        RowLayout
        {
            Layout.row: 1
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 10

            Text
            {
                id: rotateText
                Layout.column: 0
                Layout.preferredHeight: rowHeight
                Layout.preferredWidth: parent.width / 3

                verticalAlignment: Text.AlignVCenter

                font.pixelSize: fontPixelSize
                text: qsTr("Поворот:")
            }

            Text
            {
                id: rotateLeftText
                Layout.column: 1
                Layout.fillHeight: true
                Layout.preferredWidth: parent.width / 3

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                font.pixelSize: rotateText.font.pixelSize
                font.underline: true
                text: qsTr("Влево")

                MouseArea
                {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor

                    onClicked: ImageEditor.rotate(-90)
                }
            }

            Text
            {
                id: rotateRightText
                Layout.column: 2
                Layout.fillHeight: true
                Layout.preferredWidth: parent.width / 3

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                font: rotateLeftText.font
                text: qsTr("Вправо")

                MouseArea
                {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor

                    onClicked: ImageEditor.rotate(90)
                }
            }
        }

        RowLayout
        {
            Layout.row: 2
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 10

            Text
            {
                id: flipText
                Layout.column: 0
                Layout.preferredHeight: rowHeight
                Layout.preferredWidth: parent.width / 3

                verticalAlignment: Text.AlignVCenter

                font: rotateText.font
                text: qsTr("Отразить:")
            }

            Text
            {
                id: flipHorizontalText
                Layout.column: 1
                Layout.fillHeight: true
                Layout.preferredWidth: parent.width / 3

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                font.pixelSize: rotateText.font.pixelSize
                font.underline: true
                text: qsTr("Горизонтально")

                MouseArea
                {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor

                    onClicked: ImageEditor.mirrored(true, false)
                }
            }

            Text
            {
                id: flipVerticalText
                Layout.column: 2
                Layout.fillHeight: true
                Layout.preferredWidth: parent.width / 3

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                font: rotateLeftText.font
                text: qsTr("Вертикально")

                MouseArea
                {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor

                    onClicked: ImageEditor.mirrored(false, true)
                }
            }
        }

        Text
        {
            id: saveFileText
            enabled: ImageEditor.edited
            Layout.row: 3
            anchors.left: parent.left
            anchors.right: parent.right
            Layout.preferredHeight: rowHeight

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            font.pixelSize: rotateText.font.pixelSize
            font.underline: true
            text: qsTr("Сохранить изменения")

            MouseArea
            {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: saveFile()
            }
        }

        Item
        {
            Layout.row: 99
            Layout.preferredHeight: parent.height
        }
    }
}
