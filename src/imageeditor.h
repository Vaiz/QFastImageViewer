#ifndef IMAGEEDITOR_H
#define IMAGEEDITOR_H

#include <QImage>

class QQmlEngine;
class QJSEngine;
class ImageEditorPrivate;

class ImageEditor : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QImage image READ image NOTIFY imageChanged)
    Q_PROPERTY(bool edited READ edited NOTIFY editedChanged)
    Q_PROPERTY(bool readOnly READ readOnly NOTIFY readOnlyChanged)

private:
    explicit ImageEditor();

public:
    ~ImageEditor();
    static QObject *qmlSingleton(QQmlEngine *engine,
                                 QJSEngine *scriptEngine);

    QImage image() const;
    bool edited() const;
    bool readOnly() const;

public slots:
    void setImage(QObject *image);
    void rotate(const int &degree);
    void mirrored(const bool horizontal = false,
                              const bool vertical = true);
    void save();

signals:
    void imageChanged();
    void editedChanged();
    void readOnlyChanged();

private:
    void setEdited(const bool);
    void setReadOnly(const bool);

private:
    ImageEditorPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(ImageEditor)
};

#endif // IMAGEEDITOR_H
